# frag.jetzt

Nomen est omen: The app's name says it all: it stands for both the app's main purpose and the web address https://frag.jetzt

## Documentation

* [For developers](development.md)

## Credits

frag.jetzt is powered by Technische Hochschule Mittelhessen | University of Applied Sciences.
